FROM postgres:11-alpine

RUN set -ex
RUN apk add --no-cache openssl git curl postgresql-dev make gcc clang musl-dev
RUN git clone https://github.com/xocolatl/periods.git
RUN cd periods && make && make install
RUN apk del openssl git curl postgresql-dev make gcc clang musl-dev
